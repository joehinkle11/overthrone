﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ChessLogic : MonoBehaviour {
	
	private bool useMultiMouse;


	public ChessPiece[] aiPieces;

	public GameObject[] blackPieces1;
	public GameObject[] blackPieces2;
	public GameObject[] whitePieces1;
	public GameObject[] whitePieces2;
	public GameObject[,] chessBoard;

	public float spaceWidth;
	public float spaceHeight;

	private ChessPiece player1SelectedPiece;
	private ChessPiece player2SelectedPiece;

	// Use this for initialization
	void Start () {
		useMultiMouse = ApplicationModel.isMultMouseOn;
		// switch the 1d GameObject arrays used for the unity editor into one array
		chessBoard = new GameObject[8,8];
		aiPieces = new ChessPiece[16];
		for (int i = 0; i < 8; i++) {
			aiPieces [i] = blackPieces1 [i].GetComponent<ChessPiece>();
		}
		for (int i = 0; i < 8; i++) {
			aiPieces [i+8] = blackPieces2 [i].GetComponent<ChessPiece>();
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				if (i == 0) {
					chessBoard [j,0] = blackPieces1 [j];
					chessBoard [j,0].GetComponent<ChessPiece>().SetBoardPosition(j, 0);
				} else if (i == 1) {
					chessBoard [j,1] = blackPieces2 [j];
					chessBoard [j,1].GetComponent<ChessPiece>().SetBoardPosition(j, 1);
				} else if (i == 2) {
					chessBoard [j,6] = whitePieces1 [j];
					chessBoard [j,6].GetComponent<ChessPiece>().SetBoardPosition(j, 6);
				} else {
					chessBoard [j,7] = whitePieces2 [j];
					chessBoard [j,7].GetComponent<ChessPiece>().SetBoardPosition(j, 7);
				}
			}
		}

	}

	void Update() {
		if (ApplicationModel.isMultiPlayer == false) {
			if (Random.value < .05f +ApplicationModel.diff) {
				ChessAI.makeNextMove (chessBoard,aiPieces, this);
			}
		}
	}

	public int[] GetBoardPosition(GameObject gameObj)
	{
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (gameObj == chessBoard [i, j]) {
					return new int[]{i,j};
				}
			}
		}
		return new int[]{-1,-1};
	}

	public void DeselectPiece(ChessPiece piece) {
		piece.Deselect ();
		// if deselection was successful, then we should set our current selection (for appropriate player) to null
		if (piece.playerOwnershipID == 1) {
			player1SelectedPiece = null;
		} else if (piece.playerOwnershipID == 2) {
			player2SelectedPiece = null;
		}
	}

	public bool SelectPiece(ChessPiece piece) {
		if (piece.Select ()){ // select the unit on the space clicked
			// if selection was successful, then we should set our current selection (for appropriate player) to that newly selected piece. 
			// if selection was successful, then we should set our current selection (for appropriate player) to that newly selected piece. 
			if (piece.playerOwnershipID == 1) {
				if (player1SelectedPiece != null) {
					player1SelectedPiece.Deselect();
				}
				player1SelectedPiece = piece;
			} else if (piece.playerOwnershipID == 2) {
				if (player2SelectedPiece != null) {
					player2SelectedPiece.Deselect();
				}
				player2SelectedPiece = piece;
			}
			return true; //successful
		}else{
			return false; //unsuccessful
		}
	}

	public bool RequestToMovePieceToBlankSpace(ChessPiece piece, int x, int y)
	{
		List<int> validMoves = piece.GetValidMoves ();
		// check if the desired position intersects with one of the valid moves
		for (int i = 0; i < validMoves.Count * .5; i++) {
			if (validMoves [i * 2] == x && validMoves [i * 2 + 1] == y) {
				// move the piece
				chessBoard[piece.boardPositionX,piece.boardPositionY] = null;
				chessBoard[x,y] = piece.gameObject;
				piece.MoveToBoardPosition (x, y);
				piece.Move ();
				// after moving deselect the piece if it is selected
				if (piece.isSelected()) {
					DeselectPiece (piece);
				}
				return true;
			}
		}
		return false;
	}
	public bool RequestToMovePieceToSpaceWithEnemy(ChessPiece piece, int x, int y)
	{
		List<int> validMoves = piece.GetValidAttacks ();
		// check if the desired position intersects with one of the valid moves
		for (int i = 0; i < validMoves.Count * .5; i++) {
			if (validMoves [i * 2] == x && validMoves [i * 2 + 1] == y) {
				// move the piece
				chessBoard[piece.boardPositionX,piece.boardPositionY] = null;
				chessBoard [x, y].GetComponent<ChessPiece> ().destroyPiece ();
				chessBoard[x,y] = piece.gameObject;
				piece.MoveToBoardPosition (x, y);
				piece.Move ();
				// after moving deselect the piece if it is selected
				if (piece.isSelected()) {
					DeselectPiece (piece);
				}
				return true;
			}
		}
		return false;
	}
	bool RequestToMovePieceToSpaceWithAlly(ChessPiece piece, int x, int y)
	{
		return false;
	}

	// *****************
	// SpaceSelected:
	//
	// If the player ID is 1 or 2, only pieces with those player ownship ID should be able to be moved.
	// If the player ID is -1, it is a wildcard and this space is selected regardless of player ID.
	// *****************
	void SpaceSelected(int x, int y, int playerID)
	{
		// There is a piece on this space
		if (chessBoard [x, y] != null) {
			ChessPiece piece = chessBoard [x, y].GetComponent<ChessPiece> ();
			// check is we should attack
			if (piece.playerOwnershipID != playerID) {
				if ((playerID == 1 || playerID == -1) && player1SelectedPiece != null && piece.playerOwnershipID == 2) {
					if (RequestToMovePieceToSpaceWithEnemy (player1SelectedPiece, x, y)) { // successful attack
						return;
					}
				} else if ((playerID == 2 || playerID == -1) && player2SelectedPiece != null && piece.playerOwnershipID == 1) {
					if (RequestToMovePieceToSpaceWithEnemy (player2SelectedPiece, x, y)) { // successful attack
						return;
					}
				}
			}
			// select this unit
			// if we are in jurisdiction to select this unit
			if (piece.playerOwnershipID == playerID || playerID == -1) {
				if (piece.isSelected ()){ // if the piece is already selected...
					DeselectPiece(piece);
				} else {
					SelectPiece(piece);
				}
				return;
			}
		} else {
			if (playerID == 1 || playerID == -1) {
				if (player1SelectedPiece != null) { //maybe you want to move a piece here?
					if (!RequestToMovePieceToBlankSpace (player1SelectedPiece, x, y)) { // if this fails and you are a "-1" player, check if you want to move the other piece
						if (playerID == -1 && player2SelectedPiece != null) { //maybe you want to move a piece here?
							RequestToMovePieceToBlankSpace(player2SelectedPiece, x, y);
						}
					}
				}else if (playerID == -1 && player2SelectedPiece != null) { //maybe you want to move a piece here?
					RequestToMovePieceToBlankSpace(player2SelectedPiece, x, y);
				}
			}else if (playerID == 2 || playerID == -1) {
				if (player2SelectedPiece != null) { //maybe you want to move a piece here?
					RequestToMovePieceToBlankSpace(player2SelectedPiece, x, y);
				}
			}

		}
	}

	public GameObject GetPieceAtSpace(int x, int y)
	{
		if (x >= 0 && x <= 7 && y >= 0 && y <= 7) {
			return chessBoard [x, y];
		} else {
			return null;
		}
	}

	public char GetPieceHover(GameObject cursor)
	{
		int spaceClickedX = (int) Mathf.Round((cursor.transform.position.x - transform.position.x) / spaceWidth / transform.localScale.x + 3.5f);
		int spaceClickedY = (int) Mathf.Round(3.5f - (cursor.transform.position.y - transform.position.y)/spaceHeight/transform.localScale.y);

		if (spaceClickedX >= 0 && spaceClickedX <= 7 && spaceClickedY >= 0 && spaceClickedY <= 7) {
			GameObject chessPieceHover = chessBoard [spaceClickedX, spaceClickedY];
			if (chessPieceHover == null) {
				return ' ';
			} else {
				return chessPieceHover.GetComponent<ChessPiece>().pieceType;
			}
		} else {
			return ' ';
		}
	}

	public void ClickHandle(int playerid)
	{
        // figure out which space you clicked

		if (useMultiMouse) {
			// NEW CODE
	        //TODO: seperate cursor input

	        GameObject[] cursors = new GameObject[3]; //just so index can match player number
	        cursors[1] = GameObject.Find("cursor1");
	        cursors[2] = GameObject.Find("cursor2");

	        if (cursors[1] == null) {print("cursor1 is null"); }

	        if (cursors[2] == null) { print("cursor2 is null"); }

	        //for (int i = 1; i <= 2; i++)
	        //{
	            CursorHandler handler = cursors[playerid].GetComponent<CursorHandler>();

	            if (handler == null) { print("ERROR: NULL HANDLER"); } else { print("handler good to go"); }

	            if (handler.isClicking())
	            {
	                int spaceClickedX = (int) Mathf.Round((cursors[playerid].transform.position.x - transform.position.x) / spaceWidth / transform.localScale.x + 3.5f);
	                int spaceClickedY = (int) Mathf.Round(3.5f - (cursors[playerid].transform.position.y - transform.position.y)/spaceHeight/transform.localScale.y);

	                if (spaceClickedX >= 0 && spaceClickedX <= 7 && spaceClickedY >= 0 && spaceClickedY <= 7)
	                {
	                    SpaceSelected(spaceClickedX, spaceClickedY, playerid);
	                }
	            }
	        //}

	        /*if (cursor1.GetComponent<CursorHandler>().isClicking())
	        {
	            int spaceClickedX = (int)Mathf.Round((cursor1.transform.position.x - transform.position.x) / spaceWidth / transform.localScale.x + 3.5f);
	            int spaceClickedY = (int)Mathf.Round(3.5f - (cursor1.transform.position.y - transform.position.y) / spaceHeight / transform.localScale.y);

	            if (spaceClickedX >= 0 && spaceClickedX <= 7 && spaceClickedY >= 0 && spaceClickedY <= 7)
	            {
	                SpaceSelected(spaceClickedX, spaceClickedY, -1);
	            }
	        }

	        if (cursor2.GetComponent<CursorHandler>().isClicking())
	        {
	            int spaceClickedX = (int)Mathf.Round((cursor2.transform.position.x - transform.position.x) / spaceWidth / transform.localScale.x + 3.5f);
	            int spaceClickedY = (int)Mathf.Round(3.5f - (cursor2.transform.position.y - transform.position.y) / spaceHeight / transform.localScale.y);

	            if (spaceClickedX >= 0 && spaceClickedX <= 7 && spaceClickedY >= 0 && spaceClickedY <= 7)
	            {
	                SpaceSelected(spaceClickedX, spaceClickedY, -1);
	            }
	        }*/
		}
        
    }

	void OnMouseDown() {
		if (!useMultiMouse) {
			// OLD CODE FOR TESTING
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			int spaceClickedX = (int)Mathf.Round ((ray.GetPoint (0).x - transform.position.x) / spaceWidth / transform.localScale.x + 3.5f);
			int spaceClickedY = (int)Mathf.Round (3.5f - (ray.GetPoint (0).y - transform.position.y) / spaceHeight / transform.localScale.y);
			if (spaceClickedX >= 0 && spaceClickedX <= 7 && spaceClickedY >= 0 && spaceClickedY <= 7) {
				
				SpaceSelected (spaceClickedX, spaceClickedY, -1);
			}
		}
	}


    void OnGUI()
	{
		if (useMultiMouse) {
			// don't need to show this anymore
			/*
			GUI.Label (new Rect (100, 100, 200, 100), "OS MOUSEPOS:\nx: " + Camera.main.ScreenPointToRay (Input.mousePosition).GetPoint (0).x +
			"\ny: " + Camera.main.ScreenPointToRay (Input.mousePosition).GetPoint (0).y);

			GameObject cursor1 = GameObject.Find ("cursor1");
			GameObject cursor2 = GameObject.Find ("cursor2");

			GUI.Label (new Rect (100, 200, 200, 100), "cursor1 MOUSEPOS:\nx: " + cursor1.transform.position.x +
			"\ny: " + cursor1.transform.position.y);
			if (cursor1.GetComponent<CursorHandler> ().isClicking ()) {
				GUI.Label (new Rect (200, 200, 200, 100), "cursor1 clicked");
			}


			GUI.Label (new Rect (100, 300, 200, 100), "cursor2 MOUSEPOS:\nx: " + cursor2.transform.position.x +
			"\ny: " + cursor2.transform.position.y);
			if (cursor2.GetComponent<CursorHandler> ().isClicking ()) {
				GUI.Label (new Rect (200, 200, 200, 100), "cursor2 clicked");
			}
			*/
		}
	}

}
