﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChessAI {

	public static void makeNextMove(GameObject[,] chessBoard, ChessPiece[] myPieces, ChessLogic chessLogic){
		for (int i = 0; i < 16; i++) {
			if (myPieces [i] != null) {
				if (!myPieces [i].isCoolingDown ()) {
					// kill king
					List<int> attacks = myPieces [i].GetValidAttacks ();
					if (attacks.Count > 0) {
						for (int j = 0; j < attacks.Count * .5; j++) {
							if (attacks [j * 2] > 0 && attacks [j * 2] < 8 && attacks [j * 2 + 1] > 0 && attacks [j * 2 + 1] < 8) {
								if (chessBoard [attacks [j * 2], attacks [j * 2 + 1]] != null) {
									if (chessBoard [attacks [j * 2], attacks [j * 2 + 1]].GetComponent<ChessPiece> ().pieceType == 'k') {
										if (chessBoard [attacks [j * 2], attacks [j * 2 + 1]].GetComponent<ChessPiece> ().playerOwnershipID != 2) {
											chessLogic.RequestToMovePieceToSpaceWithEnemy (myPieces [i], attacks [j * 2], attacks [j * 2 + 1]);
											return;
										}
									}
								}
							}
						}
					}
					// other
					if (attacks.Count > 0) {
						for (int j = 0; j < attacks.Count * .5; j++) {
							if (Random.value*attacks.Count < 1) {
								if (attacks [j * 2] > 0 && attacks [j * 2] < 8 && attacks [j * 2 + 1] > 0 && attacks [j * 2 + 1] < 8) {
									if (chessBoard [attacks [j * 2], attacks [j * 2 + 1]] != null) {
										if (chessBoard [attacks [j * 2], attacks [j * 2 + 1]].GetComponent<ChessPiece> ().playerOwnershipID != 2) {
											chessLogic.RequestToMovePieceToSpaceWithEnemy (myPieces [i], attacks [j * 2], attacks [j * 2 + 1]);
											return;
										}
									}
								}
							}
						}
					}
					List<int> moves = myPieces [i].GetValidMoves ();
					if (moves.Count > 0) {
						for (int j = 0; j < moves.Count * .5; j++) {
							if (Random.value*moves.Count < 1) {
								if (moves [j * 2] > 0 && moves [j * 2] < 8 && moves [j * 2 + 1] > 0 && moves [j * 2 + 1] < 8) {
									if (chessBoard [moves [j * 2], moves [j * 2 + 1]] == null) {
										chessLogic.RequestToMovePieceToBlankSpace (myPieces [i], moves [j * 2], moves [j * 2 + 1]);
										return;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
