﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChessPiece : MonoBehaviour {

	public AudioSource moveSound;
	public AudioSource dieSound;
	public Sprite queenSpriteToTurnInto;
	public RuntimeAnimatorController queenControllerToTurnInto;

	public GameObject chessBoard;
	private ChessLogic chessLogic;
	public int boardPositionX = -1;
	public int boardPositionY = -1;

	public int playerOwnershipID; // either 1 or 2
	public char pieceType; // p=pawn, r=rook, n=knight, k=king, q=queen, b=bishop
	public int direction; // -1=up, 1=down

	private bool selected = false;

	private bool haveNotMoved = true;

    //how many tics left before the piece can be moved again
    private int cooldown = 0;

    //how many tics the cooldown is
    private const int COOLDOWN_LENGTH = 180;

    //pointer to Animator controller
    Animator animator;

    //gives the animation time to finish after piece is dead
    private bool isDead = false;
    private int deathdelay = 40;

    //variables for drawing cooldown bar
    private static Texture2D _staticRectTexture;
    private static GUIStyle _staticRectStyle;

    //function to draw cooldown bar
    public static void GUIDrawRect(Rect position, Color color)
    {
        if(_staticRectTexture == null)
        {
            _staticRectTexture = new Texture2D(1, 1);
        }

        if(_staticRectStyle == null)
        {
            _staticRectStyle = new GUIStyle();
        }

        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);
    }

    //display cooldown bar to each game object
    public void OnGUI()
    {
        if (cooldown > 0)
        {
            int drawCooldown = cooldown / 3;
            Vector3 workingPosition = transform.position;
            workingPosition.y *= -1;
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(workingPosition);
            GUIDrawRect(new Rect(screenPosition.x - 30, screenPosition.y + 34, 59, 8), Color.black);
            GUIDrawRect(new Rect(screenPosition.x - 28, screenPosition.y + 35, drawCooldown, 5), Color.red);
        }
    }

	public bool isCoolingDown() {
		return cooldown > 0;
	}

	// Use this for initialization
	void Start () {
		chessLogic = chessBoard.GetComponent<ChessLogic> ();
        animator = GetComponent<Animator>();
    }

	// Update is called once per frame
	void Update () {

        if (isDead)
        {
            deathdelay -= 1;

            if (deathdelay <= 0)
            {
                Destroy(gameObject);
                // if you are a king
                if (pieceType == 'k')
                {
                    if (playerOwnershipID == 1)
                    {
                        ApplicationModel.winner = 2;
                    }
                    else {
                        ApplicationModel.winner = 1;
                    }
                    Application.LoadLevel("GameOver");
                }
            }
        }

		PositionAtBoardPosition ();

        if (cooldown > 0)
        {
            cooldown -= 1;
            //make translucent (i.e. 50% transparency) if it's still on cooldown
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);

        }
        else
        {
            //otherwise, return it to normal
            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        }
	}

	public void Move()
	{
		moveSound.Play ();
        cooldown = COOLDOWN_LENGTH;
	}

	public bool isSelected()
	{
		return selected;
	}

	// note: this must always succeed due to logic in chesslogic
	public void Deselect()
	{
		transform.localScale = new Vector3 (transform.localScale.x*.8f,transform.localScale.y*.8f,transform.localScale.z*.8f);
		selected = false;
	}

	// note: you may fail this method (return false and not select the piece) and the logic and chesslogic will handle it
	public bool Select()
	{
        if(cooldown <= 0 && isDead == false){
            transform.localScale = new Vector3(transform.localScale.x * 1.25f, transform.localScale.y * 1.25f, transform.localScale.z * 1.25f);
            selected = true;
        }
        else
        {
            selected = false;
        }
		return selected; // if this is false, we know it failed to select
	}

	public void MoveToBoardPosition(int x, int y)
	{
		haveNotMoved = false;
		SetBoardPosition (x, y);
		PositionAtBoardPosition ();
	}

	public void PositionAtBoardPosition()
	{
        float targetx = chessBoard.transform.position.x + (chessLogic.spaceWidth * (boardPositionX - 3.5f)) * chessBoard.transform.localScale.x;
        float targety = chessBoard.transform.position.y + (chessLogic.spaceHeight * (3.5f - boardPositionY)) * chessBoard.transform.localScale.y;

        if (isDead == false)
		    transform.position = new Vector3 ((targetx + transform.position.x)*.5f,(targety + transform.position.y)*.5f,0);

        //animation transition template
        /*if (animator != null)
            animator.SetInteger("parametername", 1);*/
    }

	public void SetBoardPosition(int x, int y)
	{
		boardPositionX = x;
		boardPositionY = y;
		// check pawn promotion
		if (pieceType == 'p') {
			if (playerOwnershipID == 1) {
				if (y == 0) {
					pieceType = 'q';
					SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
					if (spriteRenderer != null) {
						spriteRenderer.sprite = queenSpriteToTurnInto;
					}
					Animator animator = GetComponent<Animator>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
					if (animator != null) {
						animator.runtimeAnimatorController = queenControllerToTurnInto;
					}
				}
			}else if (playerOwnershipID == 2) {
				if (y == 7) {
					pieceType = 'q';
					SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
					if (spriteRenderer != null) {
						spriteRenderer.sprite = queenSpriteToTurnInto;
					}
					Animator animator = GetComponent<Animator>(); // we are accessing the SpriteRenderer that is attached to the Gameobject
					if (animator != null) {
						animator.runtimeAnimatorController = queenControllerToTurnInto;
					}
				}
			}
		}
	}

	public List<int> GetValidMoves()
	{
		if (pieceType == 'p') {
			List<int> validMoves = new List<int> ();
			validMoves.Add (boardPositionX);
			validMoves.Add (boardPositionY + direction); // forward movement by one space
			if (haveNotMoved && chessLogic.GetPieceAtSpace(boardPositionX,boardPositionY+direction) == null) { // if you haven't moved and if the spot you are checking is not off the board and if there is nobody in the way
				validMoves.Add (boardPositionX);
				validMoves.Add (boardPositionY + direction * 2); // forward movement by two spaces if you haven't moved
			}
			// add en passant
			return validMoves;
		} else if (pieceType == 'r') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + i); // forward movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - i); // backward movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY); // left movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY); // right movement by "i" spaces (this will cover the entire board)
			}
			// add castling
			return validMoves;
		} else if (pieceType == 'b') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			return validMoves;
		} else if (pieceType == 'q') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + i); // forward movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - i); // backward movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY); // left movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY); // right movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY + i) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			for (int i = 1; i <= 7; i++) {
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY - i) != null) { break; }
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
			}
			return validMoves;
		} else if (pieceType == 'k') {
			List<int> validMoves = new List<int> ();
			validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + 1); // up
			validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - 1); // down
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY); // left
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY); // right
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY + 1); // diagonal
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY - 1); // diagonal
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY + 1); // diagonal
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY - 1); // diagonal
			return validMoves;
		} else if (pieceType == 'n') {
			List<int> validMoves = new List<int> ();
			validMoves.Add (boardPositionX+2); validMoves.Add (boardPositionY + 1);
			validMoves.Add (boardPositionX+2); validMoves.Add (boardPositionY - 1);
			validMoves.Add (boardPositionX-2); validMoves.Add (boardPositionY + 1);
			validMoves.Add (boardPositionX-2); validMoves.Add (boardPositionY - 1);
			validMoves.Add (boardPositionX-1); validMoves.Add (boardPositionY + 2);
			validMoves.Add (boardPositionX-1); validMoves.Add (boardPositionY - 2);
			validMoves.Add (boardPositionX+1); validMoves.Add (boardPositionY + 2);
			validMoves.Add (boardPositionX+1); validMoves.Add (boardPositionY - 2);
			return validMoves;
		}else{
			List<int> validMoves = new List<int> ();
			return validMoves;
		}
	}

	public void destroyPiece()
	{
		// do any last minute things before destroying...
		dieSound.Play ();

        if(isSelected())
            chessBoard.GetComponent<ChessLogic>().DeselectPiece(this);

        //move off center so you can see capturing piece
        transform.position = new Vector3(transform.position.x, transform.position.y+(chessLogic.spaceWidth/3)*direction, transform.position.z);

        //draw behind other pieces
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = -7;

        //reset cooldown so it doesn't have cooldown indicator
        cooldown = -1;

        //trigger death animation
        if(animator != null)
            animator.SetInteger("dead", 1);

        isDead = true;
	}

	public List<int> GetValidAttacks()
	{
		if (pieceType == 'p') {
			List<int> validMoves = new List<int> ();
			if (chessLogic.GetPieceAtSpace(boardPositionX - 1, boardPositionY + direction) != null && chessLogic.GetPieceAtSpace(boardPositionX - 1, boardPositionY + direction).GetComponent<ChessPiece> ().playerOwnershipID != playerOwnershipID) {
				validMoves.Add (boardPositionX - 1);
				validMoves.Add (boardPositionY + direction); // forward movement and left by one space to take enenmy
			}
			if (chessLogic.GetPieceAtSpace(boardPositionX + 1, boardPositionY + direction) != null && chessLogic.GetPieceAtSpace(boardPositionX + 1, boardPositionY + direction).GetComponent<ChessPiece> ().playerOwnershipID != playerOwnershipID) {
				validMoves.Add (boardPositionX + 1);
				validMoves.Add (boardPositionY + direction); // forward movement and left by one space to take enenmy
			}
			// add en passant
			return validMoves;
		} else if (pieceType == 'r') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + i); // forward movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - i); // backward movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY - i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY); // left movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY); // right movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY) != null) { break; }
			}
			// add castling
			return validMoves;
		} else if (pieceType == 'b') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY - i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY - i) != null) { break; }
			}
			return validMoves;
		} else if (pieceType == 'q') {
			List<int> validMoves = new List<int> ();
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + i); // forward movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - i); // backward movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX, boardPositionY - i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY); // left movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY); // right movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX + i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX + i, boardPositionY - i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY + i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY + i) != null) { break; }
			}
			for (int i = 1; i <= 7; i++) {
				validMoves.Add (boardPositionX - i); validMoves.Add (boardPositionY - i); // diagonal movement by "i" spaces (this will cover the entire board)
				if (chessLogic.GetPieceAtSpace (boardPositionX - i, boardPositionY - i) != null) { break; }
			}
			return validMoves;
		} else if (pieceType == 'k') {
			List<int> validMoves = new List<int> ();
			validMoves.Add (boardPositionX); validMoves.Add (boardPositionY + 1); // up
			validMoves.Add (boardPositionX); validMoves.Add (boardPositionY - 1); // down
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY); // left
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY); // right
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY + 1); // diagonal
			validMoves.Add (boardPositionX + 1); validMoves.Add (boardPositionY - 1); // diagonal
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY + 1); // diagonal
			validMoves.Add (boardPositionX - 1); validMoves.Add (boardPositionY - 1); // diagonal
			return validMoves;
		} else if (pieceType == 'n') {
			List<int> validMoves = new List<int> ();
			validMoves.Add (boardPositionX+2); validMoves.Add (boardPositionY + 1);
			validMoves.Add (boardPositionX+2); validMoves.Add (boardPositionY - 1);
			validMoves.Add (boardPositionX-2); validMoves.Add (boardPositionY + 1);
			validMoves.Add (boardPositionX-2); validMoves.Add (boardPositionY - 1);
			validMoves.Add (boardPositionX-1); validMoves.Add (boardPositionY + 2);
			validMoves.Add (boardPositionX-1); validMoves.Add (boardPositionY - 2);
			validMoves.Add (boardPositionX+1); validMoves.Add (boardPositionY + 2);
			validMoves.Add (boardPositionX+1); validMoves.Add (boardPositionY - 2);
			return validMoves;
		}else{
			List<int> validMoves = new List<int> ();
			return validMoves;
		}
	}
}
