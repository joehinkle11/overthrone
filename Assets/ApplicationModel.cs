﻿
public class ApplicationModel {

	public static bool isMultiPlayer = false;


	public static bool isMultMouseOn = false;

	public static int winner = 0;

	public static int diff = 0;

	private static RawMouseDriver.RawMouseDriver mousedriver;

	public static RawMouseDriver.RawMouseDriver getRawMouseDriverFromApplicationModel() {
		if (mousedriver != null) {
			return mousedriver;
		} else {
			mousedriver = new RawMouseDriver.RawMouseDriver ();
			return mousedriver;
		}
	}

}
