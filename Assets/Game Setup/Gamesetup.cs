﻿// summary//
//GameOver.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using System.Collections;
//public class UIControl : MonoBehavior;
public class Gamesetup : MonoBehaviour
{

    public Texture backgroundTexture;
    public float guiPlacementY;

    void OnGUI()
	{
		Cursor.visible = true;

		//Display Background Texture//
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.width), backgroundTexture);


		GUIStyle style = new GUIStyle (GUI.skin.label);
		style.fontSize = 32;
		style.alignment = TextAnchor.MiddleCenter;

		string myStr = "Do you have two mice plugged in?\n On some systems with only one mouse,\n" + 
                            "performance can be improved by using \"One-Mouse\" Mode.";
			

		GUI.Label (new Rect (0, Screen.height * .05f, Screen.width, Screen.height * .5f), myStr,style);

		//Dispaly out buttons
		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .6f, Screen.width * .5f, Screen.height * .1f), "One-Mouse Mode"))
		{
			ApplicationModel.isMultMouseOn = false;
			Application.LoadLevel("MainMenu");
		}
		//Dispaly out buttons
		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .8f, Screen.width * .5f, Screen.height * .1f), "Multi-Mouse Mode"))
		{
			ApplicationModel.isMultMouseOn = true;
			ApplicationModel.getRawMouseDriverFromApplicationModel ();
			Application.LoadLevel("MainMenu");
		}



    }
}