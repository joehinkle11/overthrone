﻿using UnityEngine;
using System.Collections;

public class EscapeToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update() {

        if (Input.GetKeyDown(KeyCode.Escape)) {
            Cursor.visible = true;
            Application.LoadLevel("MainMenu");
        }
    }
}
