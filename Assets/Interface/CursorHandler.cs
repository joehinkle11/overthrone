﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RawMouseDriver;
using RawInputSharp;

public class CursorHandler : MonoBehaviour
{
	private bool useMultiMouse = false;

    public int mousenumber;

	public TextMesh hoverText;

	private ChessLogic chessLogic;

    RawMouseDriver.RawMouseDriver mousedriver;

	private bool thisMouseInUse;

    //Main Camera, which should have view boundaries, which are used to keep cursors from going off screen
    ScreenBounds bounds;

    //list of clickable pieces (IS THIS NEEDED?)
    //List<GameObject> pieces;

    // Use this for initialization
    void Start()
    {
		chessLogic = GameObject.Find ("chessboard").GetComponent<ChessLogic> ();
		useMultiMouse = ApplicationModel.isMultMouseOn;
		hoverText.text = "";

		if (mousenumber == 2) {
			thisMouseInUse = ApplicationModel.isMultiPlayer;
		} else {
			thisMouseInUse = true;
		}

		if (useMultiMouse && thisMouseInUse) {
			mousedriver = ApplicationModel.getRawMouseDriverFromApplicationModel();
	        
			//get cursor boundaries
			bounds = GameObject.Find ("Main Camera").GetComponent<ScreenBounds> ();

			//get list of applicable pieces
			var objectArray = FindObjectsOfType<GameObject> ();

			//IS THIS NEEDED?
			/*for (int i = 0; i < objectArray.Length; i++)
	        {
	            ChessPiece script = objectArray[i].GetComponent<ChessPiece>();
	            if (script != null && script.playerOwnershipID == (mousenumber))
	            {
	                pieces.Add(objectArray[i]);
	            }
	        }*/
		} else {
			gameObject.SetActive (false);
		}
    }

    private float moveY = 0.0f;
    private float moveX = 0.0f;
    //whether left button was clicked this update
    private bool buttonclicked = false;
    //whether it's still down
    private bool buttondown = false;

    //sensitivity values (arbitrary, maybe we can adjust using mousewheel or something?
    private float sensitivityY = 0.01f;
    private float sensitivityX = 0.01f;
    private RawMouse mouse1;
    void Update()
    {
		try {
			if (useMultiMouse && thisMouseInUse) {
				//mousenumber starts at 0, while player numbers start at 1
				mousedriver.GetMouse (mousenumber - 1, ref mouse1);
				moveY += mouse1.YDelta * sensitivityY;
				moveX += mouse1.XDelta * sensitivityX;

				//update cursor position, but don't let it go out of bounds

				if (moveX != 0) {
					transform.Translate (Vector2.right * moveX);

					if (transform.position.x < bounds.bound_xmin) {
						transform.position = new Vector2 (bounds.bound_xmin, transform.position.y);
					}
					if (transform.position.x > bounds.bound_xmax) {
						transform.position = new Vector2 (bounds.bound_xmax, transform.position.y);
					}
				}

				if (moveY != 0) {
					transform.Translate (Vector2.down * moveY);
	            
					if (transform.position.y < bounds.bound_ymin) {
						transform.position = new Vector2 (transform.position.x, bounds.bound_ymin);
					}
					if (transform.position.y > bounds.bound_ymax) {
						transform.position = new Vector2 (transform.position.x, bounds.bound_ymax);
					}
				}
	        
				moveY = 0.0f;
				moveX = 0.0f;

				//handle button click
				bool[] buttons = mouse1.Buttons;
				bool downcheck = buttons [0] | buttons [1] | buttons [2];

				//see if the button was clicked this update, or if it was held down
				buttonclicked = (downcheck == true && buttondown == false) ? true : false;
	        
				buttondown = downcheck;

				if (buttonclicked) {
					chessLogic.ClickHandle (mousenumber);
				}

				// set the hovertext
				char currentPieceHover = chessLogic.GetPieceHover(gameObject);
				if (currentPieceHover == 'p') {
					hoverText.text = "Pawn";
				} else if (currentPieceHover == 'r') {
					hoverText.text = "Rook";
				} else if (currentPieceHover == 'n') {
					hoverText.text = "Knight";
				} else if (currentPieceHover == 'b') {
					hoverText.text = "Bishop";
				} else if (currentPieceHover == 'q') {
					hoverText.text = "Queen";
				} else if (currentPieceHover == 'k') {
					hoverText.text = "King";
				} else {
					hoverText.text = "";
				}

				//something's wrong in the following code block, I can't figure out what

				/*if (buttondown)
	        {
	            for(int i = 0; i < pieces.Count; i++)
	            {
	                
	                if (pieces[i].GetComponent<Collider2D>().bounds.Contains(transform.position))
	                {
	                    GameObject.Find("chessboard").GetComponent<ChessLogic>().HandlePiece(transform.position.x, transform.position.y);
	                }
	            }
	        }
	        */
			}
		} catch (NullReferenceException e) {
			hoverText.text = e.StackTrace;
		}
    }
    void OnApplicationQuit()
    {
		if (useMultiMouse) {
			mousedriver.Dispose ();
		}
    }

    public bool isClicking()
	{
		if (useMultiMouse) {
			return buttonclicked;
		} else {
			return false;
		}
    }

    //we're going to need manual debug output, since debugging external binaries is waaaaaay too much of a pain

    /*void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 20), "DEBUG MESSAGE HERE");
    }*/
}
