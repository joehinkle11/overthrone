﻿// summary//
//GameOver.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using System.Collections;
//public class UIControl : MonoBehavior;
public class Gameover : MonoBehaviour
{

    public Texture backgroundTexture;
    public float guiPlacementY;

    void OnGUI()
	{
		Cursor.visible = true;

		//Display Background Texture//
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);


		GUIStyle style = new GUIStyle (GUI.skin.label);
		style.fontSize = 32;
		style.alignment = TextAnchor.MiddleCenter;

		string myStr = "";
		if (ApplicationModel.winner == 1) {
			myStr = "Bottom player wins!";
		} else if (ApplicationModel.winner == 2) {
			myStr = "Top player wins!";
		}
			

		GUI.Label (new Rect (0, Screen.height * .2f, Screen.width, Screen.height * .1f), myStr,style);

        //Dispaly out buttons
		if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .6f, Screen.width * .5f, Screen.height * .1f), "Back"))
		{
			Application.LoadLevel("MainMenu");
		}



    }
}