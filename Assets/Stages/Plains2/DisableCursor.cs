﻿using UnityEngine;
using System.Collections;

public class DisableCursor : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //reenable the following once we get the custom cursors working:

		if (ApplicationModel.isMultMouseOn) {
			Cursor.visible = false;
		}
	}
}
