﻿// summary//
//Main menu.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//public class UIControl : MonoBehavior;
public class Menu5 : MonoBehaviour
{

    public Texture backgroundTexture;

    public float guiPlacementY;

    void OnGUI()
    {

        //Display Background Texture//
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);


        //Dispaly out buttons
        if (GUI.Button(new Rect(Screen.width * .60f, Screen.height * .2f, Screen.width * .2f, Screen.height * .1f), "BACK"))
        {
            ApplicationModel.isMultiPlayer = false;
            ApplicationModel.diff = 0;
            SceneManager.LoadScene("MainMenu");
        }


        //special thanks to Nintendo for the title menu base from Legend of Zelda
        //Thanks Vox Vulgaris for our background music 

    }
}

