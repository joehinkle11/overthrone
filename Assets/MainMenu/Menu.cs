﻿// summary//
//Main menu.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
//public class UIControl : MonoBehavior;
public class Menu : MonoBehaviour
{

    public Texture backgroundTexture;

    public float guiPlacementY;

    void Start()
    {
        Cursor.visible = true;
    }

    void OnGUI()
    {

        //Display Background Texture//
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);


        //Dispaly out buttons
        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .4f, Screen.width * .2f, Screen.height * .1f), "PLAY GAME"))
        {
            SceneManager.LoadScene("Menu2");
        }

        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .6f, Screen.width * .2f, Screen.height * .1f), "CREDITS"))
        {
            SceneManager.LoadScene("Menu5");
        }




    }
}