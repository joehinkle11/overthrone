﻿// summary//
//Main menu.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//public class UIControl : MonoBehavior;
public class Menu4 : MonoBehaviour
{

    public Texture backgroundTexture;

    public float guiPlacementY;

    void OnGUI()
    {

        //Display Background Texture//
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);


        //Dispaly out buttons
        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .2f, Screen.width * .2f, Screen.height * .1f), "Frodzin Plains"))
        {
            ApplicationModel.isMultiPlayer = true;
            SceneManager.LoadScene("Plains");
        }

        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .4f, Screen.width * .2f, Screen.height * .1f), "Zylibi Forest"))
        {
            ApplicationModel.isMultiPlayer = true;
            ApplicationModel.diff = 0;
            SceneManager.LoadScene("ForestAustin");
        }

        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .6f, Screen.width * .2f, Screen.height * .1f), "Medieval"))
        {
            ApplicationModel.isMultiPlayer = true;
            ApplicationModel.diff = 0;
            SceneManager.LoadScene("ForestJoe");
        }

        if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .2f, Screen.width * .2f, Screen.height * .1f), "Altilina Lake"))
        {
            ApplicationModel.isMultiPlayer = true;
            SceneManager.LoadScene("AltilinaLake");
        }

        if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .4f, Screen.width * .2f, Screen.height * .1f), "NahKriin Peak"))
        {
            ApplicationModel.isMultiPlayer = true;
            SceneManager.LoadScene("NahKriin Peak");
        }

        if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .6f, Screen.width * .2f, Screen.height * .1f), "Classic"))
        {
            ApplicationModel.isMultiPlayer = true;
            SceneManager.LoadScene("sc_test");
        }


    }
}