﻿using UnityEngine;
using System.Collections;

public class EscapeGame : MonoBehaviour
{
    //how many frames to ignore Esc buton
    const int IGNORELENGTH = 60;

    int ignorequit = IGNORELENGTH;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(ignorequit>0)
            ignorequit -= 1;
        if (Input.GetKeyDown(KeyCode.Escape) && ignorequit<=0)
        {
            //kills process; more straightforward than Application.Quit()
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}
