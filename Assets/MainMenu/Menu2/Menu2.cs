﻿// summary//
//Main menu.//
//Attached to Main Camera//
/// summary///
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//public class UIControl : MonoBehavior;
public class Menu2 : MonoBehaviour
{

    public Texture backgroundTexture;

    public float guiPlacementY;

    void OnGUI()
    {

        //Display Background Texture//
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);


        //Dispaly out buttons
        if (GUI.Button(new Rect(Screen.width * .25f, Screen.height * .4f, Screen.width * .2f, Screen.height * .1f), "One Player Mode"))
        {
            ApplicationModel.isMultiPlayer = false;
            ApplicationModel.diff = 0;
            SceneManager.LoadScene("Menu3");
        }

        if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .4f, Screen.width * .2f, Screen.height * .1f), "Two Player Mode"))
        {
            ApplicationModel.isMultiPlayer = false;
            ApplicationModel.diff = 0;
            SceneManager.LoadScene("Menu4");
        }



    }
}